﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using NotesWeb.Models;
using NotesWeb.Services.InterfaceService;

namespace NotesWeb.Services.DataBaseServices
{

    public class AttachmentDbService : IAttachmentService
    {
        public Attachment GetAttachment(int id)
        {
            using (var dbContext = new NotesContext())
            {
                var tmp = dbContext.Attachments.Find(id);
                return tmp;
            }



        }

        public List<Attachment> GetAttachmentsForNote(int noteId)
        {
            using (var dbContext = new NotesContext())
            {
                var tmp = dbContext.Attachments.Where(a => a.Note.NoteId == noteId);
                return tmp.ToList();
            }
        }

        public bool AddAttach(Attachment at, int noteId)
        {
            using (var dbContext = new NotesContext())
            {
                var note = dbContext.Notes.Find(noteId);
                if (note.Attachments == null) note.Attachments = new List<Attachment>();
                note.Attachments.Add(at);
                dbContext.Entry(note).State = EntityState.Modified;
                return dbContext.SaveChanges() > 0;
            }
        }

        public bool Create(Attachment at)
        {
            using (var dbContext = new NotesContext())
            {
                dbContext.Attachments.Add(at);
                return dbContext.SaveChanges() > 0;

            }
        }

        public bool Delete(int id)
        {
            using (var dbContext = new NotesContext())
            {
                var tmp = dbContext.Attachments.Find(id);
                dbContext.Attachments.Remove(tmp);
                return dbContext.SaveChanges() != 0;

            }
        }

        public bool DeleteByNote(int id)
        {
            using (var dbContext = new NotesContext())
            {
                foreach (var tmp in dbContext.Attachments.Where(a => a.Note.NoteId == id))
                {
                    dbContext.Attachments.Remove(tmp);
                }

                return dbContext.SaveChanges() != 0;

            }
        }
    }
}