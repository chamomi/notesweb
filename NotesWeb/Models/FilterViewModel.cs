﻿using System;

namespace NotesWeb.Models
{
    public class FilterViewModel
    {
        public Importance Importance { get; set; }
        public ClassType ClassType { get; set; }
        public DateTime Date { get; set; }
    }
}