using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NotesWeb.Models
{
    public class NotesContext : IdentityDbContext<User>
    {
        public NotesContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static NotesContext Create()
        {
            return new NotesContext();
        }

        public virtual DbSet<Attachment> Attachments { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
    }
}